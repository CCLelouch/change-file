﻿using System;//类似C语言中的头文件
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace ChangeFile//给程序起一个命名空间，命名空间内的内容只有调用命名空间使用语句"using ChangeFile"才可以使用
{
    internal class Program//创建一个类
    {
        static void Main(string[] args)//每个CSharp只有一个main入口函数
        {
            //C#会吧正斜杆识别为转义字符，所以路径使用正斜杠
            string path = "F:/Source/VSSource/Repos/CSharp/CsharpLearning/ChangeFile/MyData";
            //也可以加@直接粘贴
            //string path = @"F:\Source\VSSource\Repos\CSharp\CsharpLearning\ChangeFile\MyData";

            DirectoryInfo root = new DirectoryInfo(path);//通过路径传入参数创建一个实例，该实例可以操作文件
            FileInfo[] files = root.GetFiles();//获取文件信息，这些信息存放在，类型为FileInfo，名为files的数组中
            List<FileInfo> lstFile = files.ToList();//将数值转换为动态数组，动态数组顾名思义可以动态添加或删减数组内容和大小

            for (int i = 0; i < lstFile.Count; i++)//寻找文件名然后删除
            {
                string indexName = "data";//目标操作的文件
                string fileName = lstFile[i].Name;
                bool isContains = fileName.ToLower().Contains(indexName.ToLower());//统一使用小写来处理
                if (isContains)
                {
                    Console.WriteLine("File name contain data will be delete. Are you sure?");
                    Console.WriteLine("file name is " + fileName);
                    Console.WriteLine("Y/N");
                    string key;
                    key = Console.ReadLine();
                    if (key.ToLower() == "y")
                    {
                        lstFile[i].Delete();
                    }
                }
                else
                {
                    Console.WriteLine("the key word  \"data\"  not found.");
                }
            }
            Console.WriteLine("type anykey to esc");
            Console.ReadKey();//等待输入按键，程序将会在这里等待
        }
    }
}
